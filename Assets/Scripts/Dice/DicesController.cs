﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class DicesController : SingleTon<DicesController>
{
    public Dice dicePrefub;

    [Space]
    public Dice dice1;
    public Dice dice2;

    [Space]
    public GameObject buttonRollContainer;
    public Button buttonRall;

    [Space]
    public int sumValue;

    [Space]
    public Transform dicePoint1;
    public Transform dicePoint2;
    public Camera mainCamera;
    public Transform dicesViewCameraPosition;

    [Space]
    public Camera dice1Camera;
    public Camera dice2Camera;
    public CinemachineVirtualCamera cinemachineCamera;

    [Space]
    public bool isActive = false;

    private Vector3 oldPosition;
    private Quaternion oldLocalRotation;

    private int dicesStoped;
    private Player player;

    public void Start() {
        player = FindObjectOfType<Player>();

        if (buttonRall != null) {
            buttonRall.onClick.AddListener(OnDiceRollButton);
        }
    }

    public void OnDestroy() {
        if (buttonRall != null) {
            buttonRall.onClick.RemoveAllListeners();
        }
    }

    public void OnDiceStop(int value) {
        FindObjectOfType<CameraControl>().enabled = true;

        dicesStoped++;

        sumValue += value;

        if (dicesStoped == 2) {
            dice1.aFall -= OnDiceStop;
            dice2.aFall -= OnDiceStop;

            RollOver();
        }
    }

    public void StartDiceRoll() {
        FindObjectOfType<CameraControl>().enabled = false;
        FindObjectOfType<UIBackToPlayer>().enabled = false;

        isActive = true;

        if (cinemachineCamera != null) {
            //cinemachineCamera.gameObject.SetActive(false);
        }

        oldPosition = mainCamera.transform.position;
        oldLocalRotation = mainCamera.transform.rotation;

        mainCamera.transform.SetParent(null);
        mainCamera.transform.position = dicesViewCameraPosition.position;
        mainCamera.transform.rotation = dicesViewCameraPosition.rotation;

        dicesStoped = 0;
        sumValue = 0;

        dice1 = Instantiate(dicePrefub.gameObject, dicePoint1.position, Quaternion.identity).GetComponent<Dice>();
        dice2 = Instantiate(dicePrefub.gameObject, dicePoint2.position, Quaternion.identity).GetComponent<Dice>();

        dice1.gameObject.SetActive(true);
        dice2.gameObject.SetActive(true);

        dice1.aFall += OnDiceStop;
        dice2.aFall += OnDiceStop;

        buttonRollContainer.SetActive(true);
    }

    public void OnDiceRollButton() {
        buttonRollContainer.SetActive(false);

        dice1.Drop();
        dice2.Drop();
    }

    public void RestoreMainCameraPosition() {
        mainCamera.transform.position = oldPosition;
        mainCamera.transform.rotation = oldLocalRotation;
        mainCamera.transform.SetParent(player.transform);

        if (cinemachineCamera != null) {
            //cinemachineCamera.gameObject.SetActive(true);
        }
    }

    public void RollOver() {
        isActive = false;

        dice1Camera.transform.position = dice1.transform.position + Vector3.up * 4 + Vector3.left * 3 + Vector3.back * 3;
        dice1Camera.transform.LookAt(dice1.transform.position);
        dice1Camera.gameObject.SetActive(true);

        dice2Camera.transform.position = dice2.transform.position + Vector3.up * 4 + Vector3.right * 3 + Vector3.forward * 3;
        dice2Camera.transform.LookAt(dice2.transform.position);
        dice2Camera.gameObject.SetActive(true);

        RestoreMainCameraPosition();

        // Показать Dice экран;
        if (UIDicesScreen.Instance != null) {
            UIDicesScreen.Instance.Show();
        }
    }
}