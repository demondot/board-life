﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Dice : MonoBehaviour
{
    [SerializeField]private AudioClip diseFallSpund;

    public int value = 0;

    public float minSpeed = 100;
    public float maxSpeed = 500;

    public Transform side1;
    public Transform side2;
    public Transform side3;
    public Transform side4;
    public Transform side5;
    public Transform side6;

    public Rigidbody rb;
    public Collider selfCoolider;

    public Action<int> aFall;

    public bool isFall = false;

    private bool isDropped = false;

    private Quaternion direction1;
    private Quaternion direction2;
    private Quaternion direction3;

    private float speed;
    private int bounceTime;

    public void Start() {
        rb.useGravity = false;
        SetStartRotation();
        StartCoroutine(cr_Rotate());
    }

    private void SetStartRotation() {
        direction1 = GetRandomDirection(Vector3.up, Vector3.down);
        direction2 = GetRandomDirection(Vector3.left, Vector3.right);
        direction3 = GetRandomDirection(Vector3.forward, Vector3.back);

        int rnd = UnityEngine.Random.Range(1, 1000);
        while (rnd > 0) {
            rb.MoveRotation(rb.rotation * direction1);
            rb.MoveRotation(rb.rotation * direction2);
            rb.MoveRotation(rb.rotation * direction3);
            rnd--;
        }

        bounceTime = UnityEngine.Random.Range(1, 4);
    }

    private void OnCollisionEnter(Collision collision) {
        if(bounceTime > 0) {
            SoundsController.Instance.PlaySoundOnce(diseFallSpund);

            rb.AddForce(((Vector3.up * UnityEngine.Random.Range(7, 8)) +
                         (Vector3.left * UnityEngine.Random.Range(-1f, 1f)) +
                         (Vector3.forward * UnityEngine.Random.Range(-1f, 1f))) * 150f, ForceMode.Impulse);

            bounceTime--;
        }
    }

    private IEnumerator cr_Rotate() {
        direction1 = GetRandomDirection(Vector3.up, Vector3.down);
        direction2 = GetRandomDirection(Vector3.left, Vector3.right);
        direction3 = GetRandomDirection(Vector3.forward, Vector3.back);

        speed = UnityEngine.Random.Range(minSpeed, maxSpeed);

        float timerCounter = 0;

        float nextDirChangeTime = UnityEngine.Random.Range(0f, 1f);

        while (!isDropped) {
            rb.MoveRotation(rb.rotation * direction1);
            rb.MoveRotation(rb.rotation * direction2);
            rb.MoveRotation(rb.rotation * direction3);

            timerCounter += Time.deltaTime;

            if (timerCounter >= nextDirChangeTime) {
                direction1 = GetRandomDirection(Vector3.up, Vector3.down);
                direction2 = GetRandomDirection(Vector3.left , Vector3.right);
                direction3 = GetRandomDirection(Vector3.forward, Vector3.back);

                nextDirChangeTime = UnityEngine.Random.Range(0f, 1f);
                timerCounter = 0;
            }

            yield return new WaitForSeconds(Time.fixedDeltaTime);
        }
    }

    private Quaternion GetRandomDirection(Vector3 direction1, Vector3 direction2) {
        Quaternion deltaRotation = Quaternion.identity;

        switch (UnityEngine.Random.Range(0, 2)) {
            case 0:
                deltaRotation = Quaternion.Euler(direction1 * Time.deltaTime * UnityEngine.Random.Range(minSpeed, maxSpeed));
                break;
            case 1:
                deltaRotation = Quaternion.Euler(direction2 * Time.deltaTime * UnityEngine.Random.Range(minSpeed, maxSpeed));
                break;
        }

        return deltaRotation;
    }

    [ContextMenu("Drop")]
    public void Drop() {
        isDropped = true;
        rb.useGravity = true;
    }

    public void FixedUpdate() {
        if (!isFall) {
            calculateSide(side1, 1);
            calculateSide(side2, 2);
            calculateSide(side3, 3);
            calculateSide(side4, 4);
            calculateSide(side5, 5);
            calculateSide(side6, 6);
        }
    }

    private void calculateSide(Transform diceCollider, int sideValue) {
        Collider[] colls = Physics.OverlapSphere(diceCollider.position, 0.3f);

        if (!isFall && rb.velocity == Vector3.zero) {
            foreach (Collider col in colls) {
                if (col != null && col != selfCoolider && !col.isTrigger) {
                    value = sideValue;

                    isFall = true;

                    if (aFall != null) {
                        aFall(value);
                    }

                    return;
                }
            }
        }
    }
}