﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System;

public class GameCard : MonoBehaviour
{
    public bool drawGizmos = false;

    public Transform playerPos;
    public GameCardType cardType;
    public MeshRenderer frontMaterial;

    public Material Empty;

    public bool isOpened = false;


    [Space]
    [Header("Neightbours")]
    public GameCard topLeft;
    public GameCard topRight;
    public GameCard left;
    public GameCard right;
    public GameCard bottomLeft;
    public GameCard bottomRight;

    [Space]
    [Header("Arows")]
    public GameObject topLeftArrow;
    public GameObject topRightArrow;
    public GameObject leftArrow;
    public GameObject rightArrow;
    public GameObject bottomLeftArrow;
    public GameObject bottomRightArrow;

    [Space]
    [Header("Sounds")]
    [SerializeField] private AudioClip cardFlipSound;

    [Space]
    public Animator anim;
    public Action aCardWasOpened; 

    private Vector3 tmpPos;

    public bool _alreadyMeeted = false;

    public bool alreadyMeeted {
        set {
            if (value == true) {
                //frontMaterial.material = Empty;
                frontMaterial.material.SetColor(Shader.PropertyToID("_Color"), new Color(0.4f, 0.4f, 0.4f, 1f));
            }
                
            _alreadyMeeted = value;
        }
        get {
            return _alreadyMeeted;
        }
    }

    public static Vector3 OFFSET_LEFT = Vector3.left * GameField.cardHeight;
    public static Vector3 OFFSET_RIGHT = Vector3.right * GameField.cardHeight;
    public static Vector3 OFFSET_TOPLEFT = (Vector3.forward * GameField.cardHeight) +
                                           (Vector3.left * (GameField.cardHeight * 0.5f));
    public static Vector3 OFFSET_TOPRIGHT = (Vector3.forward * GameField.cardHeight) +
                                           (Vector3.right * (GameField.cardHeight * 0.5f));
    public static Vector3 OFFSET_BOTTOMLEFT = (Vector3.back * GameField.cardHeight) +
                                           (Vector3.left * (GameField.cardHeight * 0.5f));
    public static Vector3 OFFSET_BOTTOMRIGHT = (Vector3.back * GameField.cardHeight) +
                                       (Vector3.right * (GameField.cardHeight * 0.5f));

    public GameObject cardBorder;
    public MeshRenderer cardBorderMeshRenderer;

    public void SetCardAsActiveCard(bool state) {
        cardBorder.SetActive(state);

        if (state) {
            StartCoroutine(cr_change_border_alpha());
        } else {
            StopCoroutine(cr_change_border_alpha());
        }
    }

    private IEnumerator cr_change_border_alpha() {
        cardBorderMeshRenderer.material.SetColor("_Color", new Color(cardBorderMeshRenderer.material.color.r,
                                                          cardBorderMeshRenderer.material.color.g,
                                                          cardBorderMeshRenderer.material.color.b,
                                                          0));
        bool isDownAlpha = true;
        float alphaColor = 1;

        while (true) {
            cardBorderMeshRenderer.material.SetColor("_Color", new Color(cardBorderMeshRenderer.material.color.r,
                                                          cardBorderMeshRenderer.material.color.g,
                                                          cardBorderMeshRenderer.material.color.b,
                                                          alphaColor));

            if (isDownAlpha) {
                alphaColor -= Time.deltaTime;

                if(alphaColor <= 0) {
                    isDownAlpha = false;
                }
            } else {
                alphaColor += Time.deltaTime;

                if (alphaColor >= 1) {
                    isDownAlpha = true;
                }
            }
            yield return null;
        }
    }

    public void Init(bool playSound = false) {
        frontMaterial.material = cardType.cardMaterial;

        if (isOpened) {
            if (playSound) {
                SoundsController.Instance.PlaySoundOnce(cardFlipSound);
            }

            anim.SetBool("isOpened", isOpened);
            StartCoroutine(cr_isOpened());
        }
    }

    public IEnumerator cr_isOpened() {
        yield return new WaitForSeconds(1f);
        if(aCardWasOpened != null) {
            aCardWasOpened();
        }

        aCardWasOpened = null;
    }

    public void DisableCard() {
        OpenCard(true);
        alreadyMeeted = true;
    }

    public void OpenCard(bool needPlaySound) {
        if (isOpened == false) {
            if (needPlaySound) {
                SoundsController.Instance.PlaySoundOnce(cardFlipSound);
            }

            isOpened = true;

            anim.SetBool("isOpened", isOpened);

            if (aCardWasOpened != null) {
                aCardWasOpened();
            }

            aCardWasOpened = null;
        }
    }

    public bool isCardContainsInNeightbours(GameCard card) {
        if (topLeft != null && topLeft.Equals(card)) return true;
        if (topRight != null && topRight.Equals(card)) return true;
        if (bottomLeft != null && bottomLeft.Equals(card)) return true;
        if (bottomRight != null && bottomRight.Equals(card)) return true;
        if (left != null && left.Equals(card)) return true;
        if (right != null && right.Equals(card)) return true;

        return false;
    }

    public void OnDrawGizmosSelected() {
        if (drawGizmos) {
            Gizmos.color = Color.green;

            tmpPos = transform.position + OFFSET_LEFT;
            Gizmos.DrawWireSphere(tmpPos, 0.5f);
            printTextInEditor(tmpPos, "Left");

            tmpPos = transform.position + OFFSET_RIGHT;
            Gizmos.DrawWireSphere(tmpPos, 0.5f);
            printTextInEditor(tmpPos, "Right");

            tmpPos = transform.position + OFFSET_TOPLEFT;
            Gizmos.DrawWireSphere(tmpPos, 0.5f);
            printTextInEditor(tmpPos, "Top Left");

            tmpPos = transform.position + OFFSET_TOPRIGHT;
            Gizmos.DrawWireSphere(tmpPos, 0.5f);
            printTextInEditor(tmpPos, "Top Right");

            tmpPos = transform.position + OFFSET_BOTTOMLEFT;
            Gizmos.DrawWireSphere(tmpPos, 0.5f);
            printTextInEditor(tmpPos, "Bottom Left");

            tmpPos = transform.position + OFFSET_BOTTOMRIGHT;
            Gizmos.DrawWireSphere(tmpPos, 0.5f);
            printTextInEditor(tmpPos, "Bottom Right");
        }
    }

    private void printTextInEditor(Vector3 tmpPos, string text) {
        #if UNITY_EDITOR
        //Handles.Label(tmpPos, text);
        #endif
    }
}