﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using Serious07.PathFinding.AStar;

public class GameField : SingleTon<GameField>
{

    public int minimumCards = 10;
    public int finishCardMinRadius = 4;
    public int finishCardMaxRadius = 6;

    public const float cardWidth = 2;
    public const float cardHeight = 1.75f;

    public GameCard gameCardPrefub;
    public float cardW = cardWidth;
    public float cradH = cardHeight;

    [Space]
    public int cardsInRow;
    public int rowsCount;

    [Space]
    public LayerMask cardsMask;

    [Space]
    public GameCard startCard;
    public GameCard finishCard;

    [Space]
    public GameCardType startCradConfig;
    public GameCardType finishCardConfig;

    [Space]
    public List<GameCardType> badCardsConfing;
    public List<GameCardType> goodCradsConfig;

    [Space]
    public CinemachineVirtualCamera cinemachineCamera;
    public Camera mainCamera;

    [Space]
    private Vector3 startOffset;
    private GameObject tmpGameObject;

    [Space]
    private List<GameCard> cards = new List<GameCard>();
    private Player player;
    private AStarPathFinder pathFinder = new AStarPathFinder();
    private HashSet<AStarNode> nodes = new HashSet<AStarNode>();

    private HashSet<GameCardType> alreadySpawnedCardTypes = new HashSet<GameCardType>();
    private List<GameCardType> goodCardCanBeSpawned = new List<GameCardType>();
    private List<GameCardType> badCradCanBeSpawned = new List<GameCardType>();

    public void Start() {
        if (cinemachineCamera != null && cinemachineCamera.gameObject != null) {
            // cinemachineCamera.gameObject.SetActive(false);
        }

        GenerateField();

        player = FindObjectOfType<Player>();
    }

    [ContextMenu("Generate field")]
    public void GenerateField() {
        if(cards != null && cards.Count > 0) {
            mainCamera.transform.SetParent(null);
            alreadySpawnedCardTypes.Clear();
            goodCardCanBeSpawned.Clear();
            badCradCanBeSpawned.Clear();
            transform.Clear();
            cards.Clear();
            nodes.Clear();
        }

        

        startOffset = transform.position;

        float randomSeed = Random.Range(0f, 99999f);

        float xEnum = 0f;
        float yEnum = 0f;

        // Сгенерировать поле
        nodes.Clear();

        GameCardType cardType;

        GameCardType[] tmpArray = new GameCardType[goodCradsConfig.Count];
        goodCradsConfig.CopyTo(tmpArray);

        goodCardCanBeSpawned = new List<GameCardType>(tmpArray);

        tmpArray = new GameCardType[badCardsConfing.Count];
        badCardsConfing.CopyTo(tmpArray);

        badCradCanBeSpawned = new List<GameCardType>(tmpArray);

        int openedCardId = 0;

        for (float y = 0; y < rowsCount * cradH; y += cradH) {
            for (float x = 0; x < cardsInRow * cardWidth; x += cardWidth) {
                if (Mathf.PerlinNoise(xEnum + randomSeed, yEnum + randomSeed) <= 0.5f) {
                    AStarNode node = new AStarNode();
                    node.Init(new Vector2(startOffset.x + x, y));
                    nodes.Add(node);

                    tmpGameObject = Instantiate(gameCardPrefub.gameObject, gameObject.transform);
                    GameCard card = tmpGameObject.GetComponent<GameCard>();

                    cards.Add(card);

                    switch (Random.Range(0, 2)) {
                        case 0:
                            cardType = getCardTypeToSpawn(goodCardCanBeSpawned);
                            break;
                        case 1:
                            cardType = getCardTypeToSpawn(badCradCanBeSpawned);
                            break;
                        default:
                            cardType = getCardTypeToSpawn(badCradCanBeSpawned);
                            break;
                    }

                    card.cardType = cardType;

                    if (Random.Range(0, 100) < 10) {
                        card.isOpened = true;
                        openedCardId++;
                    } else {
                        card.isOpened = false;
                    }

                    if (openedCardId == 1)
                        card.Init(true);
                    else
                        card.Init(false);

                    tmpGameObject.transform.position = new Vector3(startOffset.x + x, transform.position.y, y);
                }

                xEnum++;
            }

            yEnum++;

            if (startOffset.x == 0) {
                startOffset.x = cardWidth * 0.5f;
            } else {
                startOffset.x = 0;
            }
        }

        // Задать соседей
        StartCoroutine(cr_Set_neightbours());
    }

    private GameCardType getCardTypeToSpawn(List<GameCardType> cardTypes) {
        GameCardType cardType;
        bool cardWasSpawned = false;

        cardType = cardTypes[Random.Range(0, cardTypes.Count)];

        if (cardType.canBeSpawnedOnlyOnce) {
            while (!cardWasSpawned) {
                if (alreadySpawnedCardTypes.Contains(cardType) == false) {
                    alreadySpawnedCardTypes.Add(cardType);
                    cardTypes.Remove(cardType);
                    cardWasSpawned = true;
                } else {
                    cardType = cardTypes[Random.Range(0, cardTypes.Count)];
                }
            }
        }

        return cardType;
    }

    private void RemoveUnreachebleCards() {
        if (startCard != null) {
            AStarNode startNode = findNodeByCard(startCard);
            AStarNode finishNode;

            List<AStarNode> nodesPath = new List<AStarNode>();

            List<GameCard> cardsToDestroy = new List<GameCard>();

            foreach (GameCard card in FindObjectsOfType<GameCard>()) {
                finishNode = findNodeByCard(card);

                nodesPath = new List<AStarNode>();

                nodesPath = pathFinder.FindPath(startNode, finishNode, nodes);

                if(nodesPath == null || nodesPath.Count == 0) {
                    cardsToDestroy.Add(card);
                }
            }

            int arraySize = cardsToDestroy.Count;

            for (int i = 0; i < arraySize; i++) {
                cards.Remove(cardsToDestroy[0]);
                Destroy(cardsToDestroy[0].gameObject);
                cardsToDestroy.RemoveAt(0);
            }
        }
    }

    private void MovePlayerToStart() {
        if (startCard != null) {
            Player pl = FindObjectOfType<Player>();
            pl.transform.position = startCard.playerPos.position;
            pl.startCard = startCard;
            pl.SetNearCardsActiveState(true);

            if (cinemachineCamera != null && cinemachineCamera.gameObject != null) {
                // cinemachineCamera.gameObject.SetActive(true);
            }

            if(mainCamera != null && player != null) {
                mainCamera.transform.SetParent(player.transform);
            }
        }
    }

    private void SetStartCard() {
        if(startCard == null) {
            startCard = FindObjectsOfType<GameCard>()[Random.Range(0, FindObjectsOfType<GameCard>().Length)];
            startCard.cardType = startCradConfig;
            startCard.isOpened = true;
            startCard.alreadyMeeted = true;


            FindObjectOfType<UIBackToPlayer>().enabled = false;

            Player pl = FindObjectOfType<Player>();
            pl.transform.position = new Vector3(99999, 99999, 99999);

            startCard.aCardWasOpened += MovePlayerToStart;
            startCard.Init();

            if(mainCamera != null) {
                mainCamera.transform.position = startCard.transform.position +
                                                (Vector3.up * 6) + (Vector3.back * 3); //+
                                                //Vector3.right * 3 +
                                                //Vector3.back * 3;
                mainCamera.transform.LookAt(startCard.transform.position);

                FindObjectOfType<UIBackToPlayer>().enabled = true;
            }
        }
    }

    private void SetFinishCard() {
        bool isGenerated = false;
        int trys = 0;

        if (startCard != null) {
            while (!isGenerated && trys < 100) {
                finishCard = FindObjectsOfType<GameCard>()[Random.Range(0, FindObjectsOfType<GameCard>().Length)];

                if(Vector3.Distance(startCard.transform.position, finishCard.transform.position) >= cardWidth * finishCardMinRadius &&
                   Vector3.Distance(startCard.transform.position, finishCard.transform.position) <= cardWidth * finishCardMaxRadius) {
                    isGenerated = true;
                }

                trys++;
            }

            finishCard.cardType = finishCardConfig;
            finishCard.isOpened = true;
            finishCard.Init();
        }
    }

    private IEnumerator cr_Set_neightbours() {
        FindObjectOfType<UIBackToPlayer>().enabled = false;

        yield return new WaitForSeconds(Time.fixedDeltaTime);

        // Задать соседей
        SetNeightbours();

        // Задать стартовую карту
        SetStartCard();

        // Удалить карты до которых невозможно добраться со старта
        RemoveUnreachebleCards();

        if (cards.Count >= minimumCards) {
            yield return new WaitForSeconds(Time.fixedDeltaTime);

            // задать финишную карту
            SetFinishCard();
        } else {
            GenerateField();
        }

        // Переместить игрока на стартовую позицию
        // MovePlayerToStart();
    }

    private void SetNeightbours() {
        foreach (GameCard card in FindObjectsOfType<GameCard>()) {
            AStarNode mainNode = findNodeByCard(card);
            AStarNode nearNode;

            // Левая карта
            card.left = getCard(card.transform.position + GameCard.OFFSET_LEFT);
            if(mainNode != null && card.left != null) {
                nearNode = findNodeByCard(card.left);

                if (nearNode != null) {
                    mainNode.AddClosedNode(nearNode);
                }
            }

            // Правая карта
            card.right = getCard(card.transform.position + GameCard.OFFSET_RIGHT);
            if (mainNode != null && card.right != null) {
                nearNode = findNodeByCard(card.right);

                if (nearNode != null) {
                    mainNode.AddClosedNode(nearNode);
                }
            }

            card.topLeft = getCard(card.transform.position + GameCard.OFFSET_TOPLEFT);
            if (mainNode != null && card.topLeft != null) {
                nearNode = findNodeByCard(card.topLeft);

                if (nearNode != null) {
                    mainNode.AddClosedNode(nearNode);
                }
            }

            card.topRight = getCard(card.transform.position + GameCard.OFFSET_TOPRIGHT);
            if (mainNode != null && card.topRight != null) {
                nearNode = findNodeByCard(card.topRight);

                if (nearNode != null) {
                    mainNode.AddClosedNode(nearNode);
                }
            }

            card.bottomLeft = getCard(card.transform.position + GameCard.OFFSET_BOTTOMLEFT);
            if (mainNode != null && card.bottomLeft != null) {
                nearNode = findNodeByCard(card.bottomLeft);

                if (nearNode != null) {
                    mainNode.AddClosedNode(nearNode);
                }
            }

            card.bottomRight = getCard(card.transform.position + GameCard.OFFSET_BOTTOMRIGHT);
            if (mainNode != null && card.bottomRight != null) {
                nearNode = findNodeByCard(card.bottomRight);

                if (nearNode != null) {
                    mainNode.AddClosedNode(nearNode);
                }
            }
        }
    }

    public GameCard findCardByCardType(GameCardType cardType) {
        List<GameCard> cardsOfNeededType = new List<GameCard>();

        foreach (GameCard card in FindObjectsOfType<GameCard>()) {
            if (card.cardType == cardType && card.alreadyMeeted == false) {
                cardsOfNeededType.Add(card);
            }
        }

        if (cardsOfNeededType == null || cardsOfNeededType.Count == 0) {
            return null;
        } else {
            return cardsOfNeededType[Random.Range(0, cardsOfNeededType.Count)];
        }
    }

    private AStarNode findNodeByCard(GameCard card) {
        Vector2 cardPosition = new Vector2(card.transform.position.x, card.transform.position.z);
        FloatVector2 floatCardPosition;
        floatCardPosition.x = cardPosition.x;
        floatCardPosition.y = cardPosition.y;

        foreach (AStarNode node in nodes) {
            if (node._position.x == floatCardPosition.x && node._position.y == floatCardPosition.y) {
                return node;
            }
        }

        return null;
    }

    private GameCard getCard(Vector3 startPos) {
        Collider[] tmpCollides;

        tmpCollides = Physics.OverlapSphere(startPos, 0.5f);

        if (tmpCollides != null && tmpCollides.Length > 0) {
            foreach (Collider col in tmpCollides) {
                if (col.GetComponentInParent<GameCard>() != null) {
                    return col.GetComponentInParent<GameCard>();
                }
            }
        }

        return null;
    }
}

public static class TransformEx {
    public static Transform Clear(this Transform transform) {
        foreach (Transform child in transform) {
            GameObject.Destroy(child.gameObject);
        }
        return transform;
    }
}