﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CardType", menuName = "CreateCardType", order = 0)]
public class GameCardType : ScriptableObject {
    [Header("Card Assets")]
    public Material cardMaterial;
    public Texture cardTexture;

    [Space]
    [Header("Effects")]
    public int giveHP;

    [Space]
    [Header("Cards texts")]
    public string cardName;
    public string cardDescription;
    public string cardGoodDiceRoll;
    public string cardBadDiceRoll;

    [Space]
    [Header("RollConditions")]
    public bool isNeedToRoll;
    public bool isConditionToPassEvent;
    public bool isGiveHPByDiseResult;
    public bool isLoseHPByDiseResult;

    [Space]
    [Header("Condition to pass event")]
    public LESS_OR_GREAT codition;
    public int rollValue = 5;

    [Space]
    [Header("Gameplay conditions")]
    public List<GameCardType> disableCards;
    public bool canBeSpawnedOnlyOnce = false;
    public List<GameCardType> ifSuccesfullMoveOnCard;
    public List<GameCardType> ifNotSuccesfullMoveOnCard;

    public override bool Equals(object obj) {
        if (!(obj is GameCardType))
            return false;

        return Equals((GameCardType)obj);
    }

    public bool Equals(GameCardType obj) {
        return obj.cardName == cardName && obj.cardDescription == cardDescription;
    }

    public override int GetHashCode() {
        unchecked // Overflow is fine, just wrap
        {
            int hash = 17;
            // Suitable nullity checks etc, of course :)
            hash = hash * 23 + cardName.GetHashCode();
            hash = hash * 23 + cardDescription.GetHashCode();
            return hash;
        }
    }

    public static bool operator ==(GameCardType a, GameCardType b) {
        return a.Equals(b);
    }

    public static bool operator !=(GameCardType a, GameCardType b) {
        return !a.Equals(b);
    }
}

public enum LESS_OR_GREAT {
    LESS = 0,
    GREAT = 1
}