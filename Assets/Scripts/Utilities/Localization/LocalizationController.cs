﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizationController : SingleTon<LocalizationController> {

    public List<TextLib> textLibs = new List<TextLib>();
    private TextLib currentTexts;
    public SystemLanguage defoultLng;
    public static Dictionary<String, String> keysValues = new Dictionary<string, string>();
    public Action aFilesLoaded;

    public SystemLanguage debugLng;

    // Use this for initialization
    void Awake() {
        if (Instance == this) {
            Init(Application.systemLanguage);
            DontDestroyOnLoad(this);
        } else {
            Destroy(gameObject);
        }
    }

    [ContextMenu("Load debud lng")]
    public void LoadDebugLng() {
        Init(debugLng);
    }

    public void Init(SystemLanguage lng) {
        keysValues.Clear();
        currentTexts = GetTextLib(lng);

        // Read sample data from CSV file
        string[,] grid = CSVReader.SplitCsvGrid(currentTexts.csvFile.text);

        for (int y = 0; y < grid.GetUpperBound(1); y++) {
            if (grid[0, y] != null && grid[1, y] != null && grid[0, y] != "" && grid[1, y] != "")
                keysValues.Add(grid[0, y], grid[1, y]);
        }

        foreach (KeyValuePair<string, string> kvp in keysValues) {
            Debug.LogFormat("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
        }

        if (aFilesLoaded != null)
            aFilesLoaded();
    }

    public TextLib GetTextLib(SystemLanguage lng) {
        TextLib defoultTextLib = null;

        foreach (TextLib txtLib in textLibs) {
            if (txtLib.lng == lng) {
                // TODO: КОСТЫЛЬ ДЛЯ АНГЛИЙСКОГО ЯЗЫКА В ИГРЕ!!!
                //if (Application.platform != RuntimePlatform.WindowsPlayer) {
                    return txtLib;
                //}
            } else if (txtLib.lng == defoultLng) {
                defoultTextLib = txtLib;
            }
        }

        return defoultTextLib;
    }
}

[System.Serializable]
public class TextLib {
    public TextAsset csvFile;
    public SystemLanguage lng;
}