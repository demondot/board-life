﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FieldLocalizator : MonoBehaviour {
    [HideInInspector]
    public Text textField;
    [HideInInspector]
    public TextMeshProUGUI tmpField;
    private LocalizationController localizator;

    private string fieldKey = "";

    private void Awake() {
        if (LocalizationController.Instance != null)
            LocalizationController.Instance.aFilesLoaded += Init;
    }

    private void OnDestroy() {
        if(LocalizationController.Instance != null && LocalizationController.Instance.aFilesLoaded != null)
            LocalizationController.Instance.aFilesLoaded -= Init;
    }

    private void Init() {
        if(localizator == null)
            localizator = LocalizationController.Instance;

        if (textField == null) {
            textField = GetComponent<Text>();
            if(textField != null && fieldKey == "") fieldKey = textField.text;
        }
        if (tmpField == null) {
            tmpField = GetComponent<TextMeshProUGUI>();
            if(tmpField != null && fieldKey == "") fieldKey = tmpField.text;
        }

        if (textField != null || tmpField != null)
            UpdateText();
    }

    private void OnEnable() {
        Init();
    }

    [ContextMenu("Update text")]
    public void UpdateText() {
        if (textField != null) {
            if (localizator != null && LocalizationController.keysValues.ContainsKey(fieldKey)) {
                textField.text = LocalizationController.keysValues[fieldKey];
            }
        } else if(tmpField != null) {
            if (localizator != null && LocalizationController.keysValues.ContainsKey(fieldKey)) {
                tmpField.text = LocalizationController.keysValues[fieldKey];
            }
        }
    }
}
