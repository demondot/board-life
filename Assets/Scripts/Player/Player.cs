﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Player : MonoBehaviour
{
    public GameCard startCard;
    public GameCard finishCard;
    public float speed = 2f;

    public Transform model;

    public bool isMoving = false;

    private Vector3 startPosition;
    private Vector3 finishPosition;

    public Action aPlayerStoped;

    private float height;

    private IEnumerator cr_move_player;

    [ContextMenu("Move player")]
    public void MovePlayer() {
        if(cr_move_player != null) {
            StopCoroutine(cr_move_player);
        }

        startPosition = startCard.playerPos.position;
        if (finishCard != null) {
            

            finishPosition = finishCard.playerPos.position;

            cr_move_player = cr_MovePlayer();
            StartCoroutine(cr_move_player);
        }
    }

    public void DeactivateCards() {
        if (startCard != null) {
            List<GameCard> allCards = new List<GameCard>(FindObjectsOfType<GameCard>());

            if (startCard.cardType.disableCards != null && startCard.cardType.disableCards.Count > 0) {
                foreach (GameCard card in allCards) {
                    if (startCard.cardType.disableCards.Contains(card.cardType)) {
                        card.DisableCard();
                    }
                }
            }
        }
    }

    public void OpenRandomNearCards() {
        List<GameCard> nearCards = new List<GameCard>();

        if (startCard.topLeft != null) nearCards.Add(startCard.topLeft);
        if (startCard.topRight != null) nearCards.Add(startCard.topRight);
        if (startCard.left != null) nearCards.Add(startCard.left);
        if (startCard.right != null) nearCards.Add(startCard.right);
        if (startCard.bottomLeft != null) nearCards.Add(startCard.bottomLeft);
        if (startCard.bottomRight != null) nearCards.Add(startCard.bottomRight);

        int numberCardsToOpen = UnityEngine.Random.Range(0, nearCards.Count);

        int openedCardsCount = 0;
        int openedCardNumber;

        bool firstCardOpened = false;

        while (openedCardsCount < numberCardsToOpen) {
            openedCardNumber = UnityEngine.Random.Range(0, nearCards.Count);

            if (firstCardOpened == false) {
                nearCards[openedCardNumber].OpenCard(true);
                firstCardOpened = true;
            } else {
                nearCards[openedCardNumber].OpenCard(false);
            }
            nearCards.RemoveAt(openedCardNumber);

            openedCardsCount++;
        }
    }

    public void OpenNearCards() {
        bool firstCardWasOpened = false;

        if (startCard.topLeft != null) {
            startCard.topLeft.OpenCard(!firstCardWasOpened);

            if (firstCardWasOpened == false) firstCardWasOpened = true;
        }
        if (startCard.topRight != null) {
            startCard.topRight.OpenCard(!firstCardWasOpened);

            if (firstCardWasOpened == false) firstCardWasOpened = true;
        }
        if (startCard.left != null) {
            startCard.left.OpenCard(!firstCardWasOpened);

            if (firstCardWasOpened == false) firstCardWasOpened = true;
        }
        if (startCard.right != null) {
            startCard.right.OpenCard(!firstCardWasOpened);

            if (firstCardWasOpened == false) firstCardWasOpened = true;
        }
        if (startCard.bottomLeft != null) {
            startCard.bottomLeft.OpenCard(!firstCardWasOpened);

            if (firstCardWasOpened == false) firstCardWasOpened = true;
        }
        if (startCard.bottomRight != null) {
            startCard.bottomRight.OpenCard(true);

            if (firstCardWasOpened == false) firstCardWasOpened = true;
        }
    }

    public void SetNearCardsActiveState(bool state) {
        if (startCard.topLeft != null) {
            startCard.topLeft.SetCardAsActiveCard(state);
            startCard.topLeftArrow.SetActive(state);
        }
        if (startCard.topRight != null) {
            startCard.topRight.SetCardAsActiveCard(state);
            startCard.topRightArrow.SetActive(state);
        }
        if (startCard.left != null) {
            startCard.left.SetCardAsActiveCard(state);
            startCard.leftArrow.SetActive(state);
        }
        if (startCard.right != null) {
            startCard.right.SetCardAsActiveCard(state);
            startCard.rightArrow.SetActive(state);
        }
        if (startCard.bottomLeft != null) {
            startCard.bottomLeft.SetCardAsActiveCard(state);
            startCard.bottomLeftArrow.SetActive(state);
        }
        if (startCard.bottomRight != null) {
            startCard.bottomRight.SetCardAsActiveCard(state);
            startCard.bottomRightArrow.SetActive(state);
        }
    }

    public IEnumerator cr_MovePlayer() {
        float currentTime = 0;

        isMoving = true;

        while (currentTime < 1) {
            transform.position = Vector3.Lerp(startPosition, finishPosition, currentTime);
            currentTime += Time.deltaTime * speed;

            if(currentTime < 0.35f) {
                height += Time.deltaTime * 2;
            } else if(currentTime >= 0.65f && currentTime < 1f) {
                height -= Time.deltaTime * 2;
            }

            model.transform.localPosition = new Vector3(0, 0, height);

            yield return new WaitForSeconds(Time.deltaTime);
        }

        height = 0;
        transform.position = finishPosition;
        model.transform.localPosition = Vector3.zero;

        startCard = finishCard;
        finishCard = null;
        isMoving = false;

        if(aPlayerStoped != null) {
            aPlayerStoped();
        }
    }
}