﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class PlayerControl : SingleTon<PlayerControl>
{
    private Player pl;
    public Camera gameCamera;

    [Space]

    public TextMeshProUGUI toolTipText;
    public GameObject toolTipContainer;

    [Space]
    private GameCard tmpCard;
    private GameCard tipCard;
    RaycastHit hit;

    [Space]
    [Header("Test stuff")]
    public GameCard testCard;

    [Space]
    public AudioClip playerStartMove;

    private void Start() {
        pl = FindObjectOfType<Player>();
    }

    // Update is called once per frame
    private void Update() {
        // Показ карты на кторую навели
        if (EventSystem.current.IsPointerOverGameObject() == false) {
            Ray ray = gameCamera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit)) {
                tipCard = hit.collider.GetComponentInParent<GameCard>();

                if(tipCard != null && tipCard.isOpened) {
                    toolTipContainer.SetActive(true);
                    toolTipText.text = LocalizationController.keysValues[tipCard.cardType.cardName];
                } else if(tipCard == null || tipCard.isOpened == false) {
                    toolTipContainer.SetActive(false);
                }
            }
        }

        // Управление игроком
        if (pl != null &&
            pl.isMoving == false &&
            Input.GetMouseButtonDown(0) &&
            EventSystem.current.IsPointerOverGameObject() == false &&
            ((DicesController.Instance != null &&
              DicesController.Instance.isActive == false) ||
            DicesController.Instance == null)) {
            
            Ray ray = gameCamera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit)) {
                tmpCard = hit.collider.GetComponentInParent<GameCard>();

                if (tmpCard != null &&
                    pl != null &&
                    pl.startCard != null &&
                    pl.startCard.isCardContainsInNeightbours(tmpCard)) {
                    MoveOnCard(tmpCard);
                }
            }
        }
    }

    private void MoveOnCard(GameCard card) {
        SoundsController.Instance.PlaySoundOnce(playerStartMove);

        if (card.isOpened == false) {
            pl.SetNearCardsActiveState(false);
            pl.isMoving = true;
            card.isOpened = true;
            card.aCardWasOpened += MovePlayer;
            card.Init();
            tmpCard = card;
        } else {
            pl.SetNearCardsActiveState(false);
            tmpCard = card;
            MovePlayer();
        }
    }

    [ContextMenu("Test move player on card")]
    public void TestMovePlayerOnCard() {
        MovePlayerOnCard(testCard);
    }

    public void MovePlayerOnCard(GameCard card) {
        MoveOnCard(card);
    }

    private void MovePlayer() {
        pl.SetNearCardsActiveState(false);

        pl.finishCard = tmpCard;
        pl.MovePlayer();
    }
}
