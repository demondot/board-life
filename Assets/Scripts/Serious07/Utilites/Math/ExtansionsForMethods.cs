﻿using UnityEngine;

namespace Serious07 {
    public static class ExtansionsForMethods {

        /// <summary>
        /// Конвертировать Vector2 в FloatVector2
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static FloatVector2 ConvertVec2ToFlVec2(Vector2 vector) {
            FloatVector2 vector2 = new FloatVector2();
            vector2.x = vector.x;
            vector2.y = vector.y;

            return vector2;
        }

        private static FloatVector2 FindDistanceHeading;
        private static float FindDistanceDistanceSquared;

        public static float FindDistance(Vector2 pos1, Vector2 pos2) {
            FindDistanceHeading.x = pos1.x - pos2.x;
            FindDistanceHeading.y = pos1.y - pos2.y;

            FindDistanceDistanceSquared = FindDistanceHeading.x *
                                          FindDistanceHeading.x + 
                                          FindDistanceHeading.y * 
                                          FindDistanceHeading.y;
            return Mathf.Sqrt(FindDistanceDistanceSquared);
        }

        public static float FindDistance(FloatVector2 pos1, FloatVector2 pos2) {
            FindDistanceHeading.x = pos1.x - pos2.x;
            FindDistanceHeading.y = pos1.y - pos2.y;

            FindDistanceDistanceSquared = FindDistanceHeading.x * FindDistanceHeading.x +
                                          FindDistanceHeading.y * FindDistanceHeading.y;

            return Mathf.Sqrt(FindDistanceDistanceSquared);
        }

        public static float getSquaredDistance(FloatVector2 pos1, FloatVector2 pos2) {
            FindDistanceHeading.x = pos1.x - pos2.x;
            FindDistanceHeading.y = pos1.y - pos2.y;

            FindDistanceDistanceSquared = FindDistanceHeading.x * FindDistanceHeading.x +
                                          FindDistanceHeading.y * FindDistanceHeading.y;

            return FindDistanceDistanceSquared;
        }

        public static bool isDistanceLessThen(FloatVector2 pos1,
                                               FloatVector2 pos2,
                                               float distance) {
            FindDistanceHeading.x = pos1.x - pos2.x;
            FindDistanceHeading.y = pos1.y - pos2.y;

            FindDistanceDistanceSquared = FindDistanceHeading.x * FindDistanceHeading.x +
                                          FindDistanceHeading.y * FindDistanceHeading.y;

            return FindDistanceDistanceSquared < (distance * distance);
        }

        public static bool isPointInCamView(Camera cam, Vector3 point) {
            Vector3 screenPoint = cam.WorldToViewportPoint(point);
            bool onScreen = screenPoint.z > 0 &&
                            screenPoint.x > 0 &&
                            screenPoint.x < 1 &&
                            screenPoint.y > 0 &&
                            screenPoint.y < 1;

            return onScreen;
        }
    }
}