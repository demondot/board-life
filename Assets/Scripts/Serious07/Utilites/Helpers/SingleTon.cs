﻿using UnityEngine;

namespace Serious07.Utilites {
    public class SingleTon<T> : MonoBehaviour where T : Object {
        private static T _instance;
        public static T Instance {
            get {
                if (_instance == null) _instance = FindObjectOfType<T>();
                return _instance;
            }
        }
    }
}