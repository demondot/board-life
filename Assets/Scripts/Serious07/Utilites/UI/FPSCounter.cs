﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSCounter : MonoBehaviour {
    float deltaTime = 0.0f;

    float maxFPS = 0;
    float minFPS = 999999;
    int w, h;
    float msec, fps;
    GUIStyle style;
    Rect rect;
    string text;

    private void Start() {
        style = new GUIStyle();

        RecalculateTextSizeAndPosition();
    }

    void RecalculateTextSizeAndPosition() {
        w = Screen.width;
        h = Screen.height;
        rect = new Rect(0, 0, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    }

    void Update() {
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
    }

    void OnGUI() {
        if (w != Screen.width || h != Screen.height) {
            RecalculateTextSizeAndPosition();
        }

        msec = deltaTime * 1000.0f;
        fps = 1.0f / deltaTime;

        if (maxFPS < fps) maxFPS = fps;
        if (minFPS > fps) minFPS = fps;

        text = string.Format("{0:0.0} ms Current: ({1:0.} fps) Max: {2} Min: {3}", msec, fps, maxFPS, minFPS);
        GUI.Label(rect, text, style);
    }
}
