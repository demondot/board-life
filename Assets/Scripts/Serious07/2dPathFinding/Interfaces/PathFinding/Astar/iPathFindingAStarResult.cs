﻿using System.Collections.Generic;

namespace Serious07.PathFinding.AStar {
    interface iPathFindingAStarResult {
        List<AStarNode> findPathResult { set; get; }
    }
}
