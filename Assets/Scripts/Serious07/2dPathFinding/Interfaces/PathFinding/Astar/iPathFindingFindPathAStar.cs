﻿using Serious07.PathFinding.AStar;
using System.Collections.Generic;

namespace Serious07.PathFinding {
    /// <summary>
    /// Используется для добавления метода поиска путей используя алгоритм поиска A star
    /// </summary>
    interface iPathFindingFindPathAStar {
        List<AStarNode> FindPath(AStarNode startPos, AStarNode finishPos, HashSet<AStarNode> allNodes);
    }
}
