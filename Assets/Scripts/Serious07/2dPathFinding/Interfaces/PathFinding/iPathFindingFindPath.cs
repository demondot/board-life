﻿using Serious07.PathFinding.Nodes;
using System.Collections.Generic;

namespace Serious07.PathFinding {
    /// <summary>
    /// Используется для добавления метода поиска путей
    /// </summary>
    interface iPathFindingFindPath {
        HashSet<iNode> FindPath(iNode startPos, iNode finishPos);
    }
}
