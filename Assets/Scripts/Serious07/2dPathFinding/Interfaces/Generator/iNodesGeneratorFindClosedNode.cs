﻿using UnityEngine;

using Serious07.PathFinding.Nodes;

namespace Serious07.PathFinding.Generator {
    /// <summary>
    /// Используется для добавления функции возвращающей ближайшую ноду от указанных координат
    /// </summary>
    interface iNodesGeneratorFindClosedNode {
        iNode findNode(Vector2 position);
    }
}
