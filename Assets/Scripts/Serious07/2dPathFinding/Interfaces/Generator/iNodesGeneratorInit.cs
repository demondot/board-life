﻿namespace Serious07.PathFinding.Generator {
    /// <summary>
    /// Используется для добавления метода инициализации генератора нод для поиска путей
    /// </summary>
    interface iNodesGeneratorInit {
        void Init();
    }
}
