﻿namespace Serious07.PathFinding.Generator {
    /// <summary>
    /// Используется для добавления триггера отвечающего за состояние инициализации генератора нод
    /// </summary>
    interface iNodesGeneratorIsInitialized {
        bool isInitialized { set; get; }
    }
}
