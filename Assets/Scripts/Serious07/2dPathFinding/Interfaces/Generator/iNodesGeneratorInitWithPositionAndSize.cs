﻿using UnityEngine;

namespace Serious07.PathFinding.Generator {
    /// <summary>
    /// Используется для добавления метода инициализации генератора нод для поиска путей
    /// </summary>
    interface iNodesGeneratorInitWithPositionAndSize {
        void Init(float x, float y, float width, float height);
        void Init(Vector2 position, Vector2 size);
        void Init(Rect area);
    }
}
