﻿namespace Serious07.PathFinding.Generator {
    /// <summary>
    /// Используется для добавления метода генерации у генератор нод
    /// </summary>
    interface iNodesGeneratorGenerate {
        void Generate();
    }
}