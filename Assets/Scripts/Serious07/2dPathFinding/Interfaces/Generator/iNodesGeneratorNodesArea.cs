﻿using UnityEngine;

namespace Serious07.PathFinding.Generator {
    /// <summary>
    /// Используется для добавления области генерации нод
    /// </summary>
    interface iNodesGeneratorNodesArea {
        /// <summary>
        /// Область для создания нод
        /// </summary>
        Rect nodesArea { set; get; }
    }
}