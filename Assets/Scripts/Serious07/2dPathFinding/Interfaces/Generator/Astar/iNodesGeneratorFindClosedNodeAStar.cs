﻿using UnityEngine;

using Serious07.PathFinding.AStar;

namespace Serious07.PathFinding.Generator {
    /// <summary>
    /// Используется для добавления функции возвращающей ближайшую ноду от указанных координат
    /// </summary>
    interface iNodesGeneratorFindClosedNodeAStar {
        AStarNode findNode(Vector2 position);
    }
}
