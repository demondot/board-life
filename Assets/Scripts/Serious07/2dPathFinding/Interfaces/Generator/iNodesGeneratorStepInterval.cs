﻿namespace Serious07.PathFinding.Generator {
    /// <summary>
    /// Используется для определения величины шага между нодами в генераторе Нод
    /// </summary>
    interface iNodesGeneratorStepInterval {
        float nodesStepInterval { set; get; }
    }
}
