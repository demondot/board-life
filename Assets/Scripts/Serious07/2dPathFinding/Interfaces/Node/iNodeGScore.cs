﻿namespace Serious07.PathFinding.Nodes {
    /// <summary>
    /// Используется для добавления ноде значения G
    /// </summary>
    interface iNodeGScore {
        /// <summary>
        /// Значение G Ноды
        /// </summary>
        float gScore { set; get; }
    }
}