﻿namespace Serious07.PathFinding.Nodes {
    /// <summary>
    /// Используется для добавления ноде значения F
    /// </summary>
    interface iNodeFScore {
        /// <summary>
        /// Значение F Ноды
        /// </summary>
        float fScore { set; get; }
    }
}