﻿namespace Serious07.PathFinding.Nodes {
    /// <summary>
    /// Используется для добавления метода инициализации ноды
    /// </summary>
    interface iNodeInit {
        /// <summary>
        /// Используется для инициализации переменных ноды
        /// </summary>
        void Init();
    }
}