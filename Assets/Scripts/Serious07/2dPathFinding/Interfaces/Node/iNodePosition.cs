﻿namespace Serious07.PathFinding.Nodes {
    using UnityEngine;

    /// <summary>
    /// Используется для добавления ноде позиции в 2д координатах
    /// </summary>
    interface iNodePosition {
        /// <summary>
        /// Позиция ноды в пространстве
        /// </summary>
        FloatVector2 position { set; get; }
    }
}