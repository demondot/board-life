﻿namespace Serious07.PathFinding.Nodes {
    /// <summary>
    /// Используется для добавления ноде статуса занятости агентом навигации
    /// </summary>
    interface iNodeOcupied {
        /// <summary>
        /// Логическая переменная описывающая занятость ноды агентом навигации
        /// </summary>
        bool isOcupied { set; get; }
    }
}