﻿namespace Serious07.PathFinding.Nodes {
    using UnityEngine;

    interface iNodeInitPosition {
        void Init(Vector2 position);
    }
}