﻿using UnityEngine;

namespace Serious07.PathFinding.AStar {
    using System.Collections.Generic;
    using UnityEngine;
    using Serious07.PathFinding.Generator;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System;

    /// <summary>
    /// Класс для генерации поля нод для поиска путей
    /// </summary>
    public class AStarNodesGenerator : iNodesGeneratorNodesArea,
                                        iNodesGeneratorStepInterval,
                                        iNodesGeneratorIsInitialized,
                                        iNodesGeneratorGenerate,
                                        iNodesGeneratorFindClosedNodeAStar,
                                        iNodesGeneratorInitWithPositionAndSize {

        public const float STEP_SIZE = 0.25f;
        
        /// <summary>
        /// Область для создания нод
        /// </summary>
        public Rect nodesArea { get; set;}

        /// <summary>
        /// Ноды
        /// </summary>
        public HashSet<AStarNode> nodes = new HashSet<AStarNode>();
        public Dictionary<FloatVector2, AStarNode> nodesHash = new Dictionary<FloatVector2, AStarNode>();

        /// <summary>
        /// Расстояние между нодами
        /// </summary>
        public float nodesStepInterval { get; set; }

        /// <summary>
        /// Генератор нод проинициализирован?
        /// </summary>
        public bool isInitialized { get; set; }

        public virtual void Generate() {

        }

        public virtual void Init() {
            nodesStepInterval = STEP_SIZE;

            isInitialized = true;
        }

        private AStarNode SetNodesNightboursTmpNode;

        private Vector2 leftPos;
        private Vector2 rightPos;
        private Vector2 downPos;
        private Vector2 upPos;
        private Vector2 topLeftPos;
        private Vector2 topRightPos;
        private Vector2 bottomLeftPos;
        private Vector2 bottomRightPos;
        private Vector2 setNodesNightboursTmpPos;

        private FloatVector2 pos;

        public Action<AStarNode> aNodeWasAdded;
        public Action<AStarNode> aNodeWasRemoved;

        public void AddNode(AStarNode node) {
            if (!nodes.Contains(node)) {
                nodes.Add(node);
            }

            if (!nodesHash.ContainsKey(node._position)) {
                nodesHash.Add(node._position, node);
            }

            aNodeWasAdded(node);
        }

        public void RemoveNode(AStarNode node) {
            if(nodes.Contains(node))
                nodes.Remove(node);
            if(!nodesHash.ContainsKey(node._position))
                nodesHash.Remove(node._position);

            if (aNodeWasRemoved != null) {
                aNodeWasRemoved(node);
            }
        }

        public virtual void SetNodesNightbours() {
            if (nodes != null && nodes.Count > 0) {
                leftPos = Vector2.left * nodesStepInterval;
                rightPos = Vector2.right * nodesStepInterval;
                downPos = Vector2.down * nodesStepInterval;
                upPos = Vector2.up * nodesStepInterval;
                topLeftPos = Vector2.left * nodesStepInterval +
                             Vector2.up * nodesStepInterval;
                topRightPos = Vector2.right * nodesStepInterval +
                              Vector2.up * nodesStepInterval;
                bottomLeftPos = Vector2.left * nodesStepInterval +
                                Vector2.down * nodesStepInterval;
                bottomRightPos = Vector2.right * nodesStepInterval +
                                 Vector2.down * nodesStepInterval;

                Parallel.ForEach(nodes, (node) => {
                    // Добавить ноду слева
                    setNodesNightboursTmpPos = node._position + leftPos;

                    pos = new FloatVector2();
                    pos.x = setNodesNightboursTmpPos.x;
                    pos.y = setNodesNightboursTmpPos.y;

                    AddNightbourToNode(node, pos);

                    // Добавить ноду справа
                    setNodesNightboursTmpPos = node.position + rightPos;

                    pos = new FloatVector2();
                    pos.x = setNodesNightboursTmpPos.x;
                    pos.y = setNodesNightboursTmpPos.y;

                    AddNightbourToNode(node, pos);

                    // Добавить ноду снизу
                    setNodesNightboursTmpPos = node.position + downPos;

                    pos = new FloatVector2();
                    pos.x = setNodesNightboursTmpPos.x;
                    pos.y = setNodesNightboursTmpPos.y;

                    AddNightbourToNode(node, pos);

                    // Добавить ноду сверху
                    setNodesNightboursTmpPos = node.position + upPos;

                    pos = new FloatVector2();
                    pos.x = setNodesNightboursTmpPos.x;
                    pos.y = setNodesNightboursTmpPos.y;

                    AddNightbourToNode(node, pos);

                    //// Добавить ноду слева сверху
                    //setNodesNightboursTmpPos = node.position + topLeftPos;

                    //pos = new FloatVector2();
                    //pos.x = setNodesNightboursTmpPos.x;
                    //pos.y = setNodesNightboursTmpPos.y;

                    //AddNightbourToNode(node, pos);

                    //// Добавить ноду справа сверху
                    //setNodesNightboursTmpPos = node.position + topRightPos;

                    //pos = new FloatVector2();
                    //pos.x = setNodesNightboursTmpPos.x;
                    //pos.y = setNodesNightboursTmpPos.y;

                    //AddNightbourToNode(node, pos);

                    //// Добавить ноду слева снизу
                    //setNodesNightboursTmpPos = node.position + bottomLeftPos;

                    //pos = new FloatVector2();
                    //pos.x = setNodesNightboursTmpPos.x;
                    //pos.y = setNodesNightboursTmpPos.y;

                    //AddNightbourToNode(node, pos);

                    //// Добавить ноду справа снизу
                    //setNodesNightboursTmpPos = node.position + bottomRightPos;

                    //pos = new FloatVector2();
                    //pos.x = setNodesNightboursTmpPos.x;
                    //pos.y = setNodesNightboursTmpPos.y;

                    //AddNightbourToNode(node, pos);
                });
            }
        }

        private void AddNightbourToNode(AStarNode node, FloatVector2 nodePosition) {
            nodesHash.TryGetValue(nodePosition, out SetNodesNightboursTmpNode);

            if (SetNodesNightboursTmpNode != null)
                node.AddClosedNode(SetNodesNightboursTmpNode);
        }

        /// <summary>
        /// Инициализация области генерации нод 
        /// </summary>
        /// <param name="x">x координата позиции</param>
        /// <param name="y">y координата позиции</param>
        /// <param name="width">ширина области генерации</param>
        /// <param name="height">высота области генерации</param>
        public virtual void Init(float x, float y, float width, float height) {
            nodesArea = new Rect(x, y, width, height);
            Init();
        }

        /// <summary>
        /// Инициализация области генерации нод
        /// </summary>
        /// <param name="position">Позиция области</param>
        /// <param name="size">Размер области</param>
        public virtual void Init(Vector2 position, Vector2 size) {
            nodesArea = new Rect(position, size);
            Init();
        }

        /// <summary>
        /// Инициализация области генеации нод
        /// </summary>
        /// <param name="area">Описание позиции и размеров области генерации нод</param>
        public virtual void Init(Rect area) {
            nodesArea = new Rect(area);
            Init();
        }

        #region findNode переменные
        /// <summary>
        /// Приближенная позция X ноды относительно переданных координат
        /// </summary>
        float findNodePosXNode;
        /// <summary>
        /// Приближенная позция Y ноды относительно переданных координат
        /// </summary>
        float findNodePosYNode;
        /// <summary>
        /// Результат функции findNode
        /// </summary>
        AStarNode findNodeResult;

        FloatVector2 findNodePos;
        #endregion

        /// <summary>
        /// Поиск ближайшей от координат ноды
        /// </summary>
        /// <param name="position">Позиция для поиска ноды</param>
        /// <returns></returns>
        public virtual AStarNode findNode(Vector2 position) {
            findNodePosXNode = position.x - (position.x % nodesStepInterval);
            findNodePosYNode = position.y - (position.y % nodesStepInterval);

            findNodePos = new FloatVector2();

            findNodePos.x = findNodePosXNode;
            findNodePos.y = findNodePosYNode;

            nodesHash.TryGetValue(findNodePos, out findNodeResult);

            if (findNodeResult != null)
                return findNodeResult;
            else
                return null;
        }
    }
}

[System.Serializable]
public struct FloatVector2 {
    public float x;
    public float y;

    public bool isZero() {
        return x == 0 && y == 0;
    }

    public override bool Equals(object obj) {
        if (!(obj is FloatVector2))
            return false;

        return Equals((FloatVector2)obj);
    }

    public bool Equals(FloatVector2 obj) {
        return obj.x == x && obj.y == y;
    }

    public override string ToString() {
        return "{ x: " + x + " y:" + y + "}";
    }

    public override int GetHashCode() {
        unchecked // Overflow is fine, just wrap
        {
            int hash = 17;
            // Suitable nullity checks etc, of course :)
            hash = hash * 23 + x.GetHashCode();
            hash = hash * 23 + y.GetHashCode();
            return hash;
        }
    }

    public static FloatVector2 operator +(FloatVector2 a, FloatVector2 b) {
        FloatVector2 result;
        result.x = a.x + b.x;
        result.y = a.y + b.y;

        return result;
    }

    public static FloatVector2 operator -(FloatVector2 a, FloatVector2 b) {
        FloatVector2 result;
        result.x = a.x - b.x;
        result.y = a.y - b.y;

        return result;
    }

    public static bool operator ==(FloatVector2 a, FloatVector2 b) {
        return a.Equals(b);
    }

    public static bool operator !=(FloatVector2 a, FloatVector2 b) {
        return !a.Equals(b);
    }

    public static implicit operator Vector2(FloatVector2 x) {
        return new Vector2(x.x, x.y);
    }

    public static implicit operator Vector3(FloatVector2 x) {
        return new Vector3(x.x, x.y);
    }

    //public static explicit operator FloatVector2(Vector2 x) {
    //    tmp.x = x.x;
    //    tmp.y = x.y;

    //    return tmp;
    //}

    //public static explicit operator FloatVector2(Vector3 x) {
    //    tmp.x = x.x;
    //    tmp.y = x.y;

    //    return tmp;
    //}
}