﻿namespace Serious07.PathFinding.AStar {
    using System.Collections.Generic;
    using UnityEngine;
    using Serious07.PathFinding.Nodes;

    public class AStarNode : iNode,
                                      iNodePosition,
                                      iNodeFScore,
                                      iNodeGScore,
                                      iNodeOcupied,
                                      iNodeInit,
                                      iNodeInitPosition {

        public static int currentHashId = 0;
        private int hashId;

        public AStarNode() {
            hashId = currentHashId;
            currentHashId++;
        }

        #region Реализация переменных абстрактного класса AStar
        /// <summary>
        /// Ближайшие соседние ноды семейства A*, указываются для поиска путей
        /// </summary>
        public HashSet<AStarNode> closedNodes;
        public AStarNode cameFrom;

        #endregion

        #region Реализация интерфейсов
        /// <summary>
        /// Позиция ноды в пространстве
        /// </summary>
        public FloatVector2 position { get { return _position; } set { _position = value; } }
        public FloatVector2 _position;
        /// <summary>
        /// Значение F ноды
        /// </summary>
        public float fScore { get { return _fScore; } set { _fScore = value; } }
        public float _fScore;
        /// <summary>
        /// Значение G ноды
        /// </summary>
        public float gScore { get { return _gScore; } set { _gScore = value; } }
        public float _gScore;
        /// <summary>
        /// Занята нода агентом или нет
        /// </summary>
        public bool isOcupied { get; set; }
        #endregion

        #region Инициализация ноды
        /// <summary>
        /// Инициализация ноды
        /// </summary>
        public virtual void Init() {
            _gScore = float.MaxValue;
            _fScore = float.MaxValue;
            cameFrom = null;

            if (closedNodes == null) {
                closedNodes = new HashSet<AStarNode>();
            }
        }

        /// <summary>
        /// Инициализация ноды с указанием её позиции
        /// </summary>
        /// <param name="position">2D координаты ноды в пространстве</param>
        public void Init(Vector2 position) {
            Init();

            _position.x = position.x;
            _position.y = position.y;
        }
        #endregion

        /// <summary>
        /// Очистить связи для ноды
        /// </summary>
        public void ClearClosedConnections() {
            foreach (AStarNode node in closedNodes) {
                node.RemoveClosedNode(this);
            }

            closedNodes.Clear();
        }

        public override bool Equals(object obj) {
            if (!(obj is AStarNode))
                return false;

            return Equals((AStarNode)obj);
        }

        public bool Equals(AStarNode obj) {
            return _position.Equals(obj._position);
        }

        public override int GetHashCode() {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                hash = hash * 23 + _position.x.GetHashCode();
                hash = hash * 23 + _position.y.GetHashCode();
                return hash;
            }
        }

        public void AddClosedNode(AStarNode node) {
            if (!closedNodes.Contains(node))
                closedNodes.Add(node);
        }

        public void RemoveClosedNode(AStarNode node) {
            if (closedNodes.Contains(node))
                closedNodes.Remove(node);
        }

        public AStarNode Copy() {
            AStarNode result = new AStarNode();
            result.Init(position);
            result.closedNodes = closedNodes;

            return result;
        }
    }
}