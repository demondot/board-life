﻿namespace Serious07.PathFinding.AStar {
    using System.Collections.Generic;
    using Serious07.PathFinding;
    using UnityEngine;
    using System.Linq;
    using System.Threading.Tasks;
    using System;

    public class AStarPathFinder : iPathFindingFindPathAStar, iPathFindingAStarResult
    {
        /// <summary>
        /// Результат поиска путей
        /// </summary>
        public List<AStarNode> findPathResult { get; set; }

        private HashSet<AStarNode> closedSet;
        private HashSet<AStarNode> openSet;

        private AStarNode currentNode;
        private float tentative_gScore;
        private AStarNode ReconstructPathTmpNode;

        public virtual List<AStarNode> ReconstructPath(AStarNode currentNode, AStarNode startPos) {
            // Построение и возрат результата поиска пути
            findPathResult = new List<AStarNode>();

            ReconstructPathTmpNode = currentNode;

            findPathResult.Add(ReconstructPathTmpNode);

            while (!ReconstructPathTmpNode.Equals(startPos)) {
                findPathResult.Add(ReconstructPathTmpNode.cameFrom);
                ReconstructPathTmpNode = ReconstructPathTmpNode.cameFrom;
            }

            findPathResult.Add(startPos);

            findPathResult.Reverse();

            return findPathResult;
        }

        /// <summary>
        /// Нахождения пути
        /// </summary>
        /// <param name="startPos">Начальная нода поиска пути</param>
        /// <param name="finishPos">Финальная нода поиска пути</param>
        /// <param name="nodes">Все ноды</param>
        /// <returns></returns>
        public virtual List<AStarNode> FindPath(AStarNode startPos, AStarNode finishPos, HashSet<AStarNode> nodes) {
            // Инициализация
            closedSet = new HashSet<AStarNode>();
            openSet = new HashSet<AStarNode>();

            foreach (AStarNode node in nodes) {
                node.Init();
            }

            if (openSet.Count == 0) {
                openSet.Add(startPos);
                startPos.gScore = 0;
                startPos.fScore = HeuristicCostEstimate(startPos, finishPos);
            }

            // Поиск
            while (openSet.Count > 0) {
                currentNode = getNodeWithTheLowestFScore(openSet);

                if (currentNode.Equals(finishPos)) {
                    return ReconstructPath(currentNode, startPos);
                }

                openSet.Remove(currentNode);
                closedSet.Add(currentNode);

                foreach (AStarNode neighbor in currentNode.closedNodes) {
                    if (closedSet.Contains(neighbor))
                        continue;

                    try {
                        tentative_gScore = currentNode.gScore +
                                           ExtansionsForMethods.FindDistance(currentNode._position, neighbor._position);

                        if (!openSet.Contains(neighbor))
                            openSet.Add(neighbor);
                        else
                            continue;

                        neighbor.cameFrom = currentNode;
                        neighbor.gScore = tentative_gScore;
                        neighbor.fScore = neighbor.gScore + HeuristicCostEstimate(neighbor, finishPos);
                    } catch(Exception ex) {
                        MonoBehaviour.print(ex);
                    }
                }
            }

            Debug.LogWarning("Функция поиска пути AStar не вернула результатов!!");
            return null;
        }

        AStarNode getNodeWithTheLowestFScoreResult;

        private AStarNode getNodeWithTheLowestFScore(HashSet<AStarNode> nodes) {
            getNodeWithTheLowestFScoreResult = null;
            //getNodeWithTheLowestFScoreResult = nodes.First();

            foreach (AStarNode node in nodes) {
                if (getNodeWithTheLowestFScoreResult == null) {
                    getNodeWithTheLowestFScoreResult = node;
                } else if(node._fScore < getNodeWithTheLowestFScoreResult._fScore) {
                    getNodeWithTheLowestFScoreResult = node;
                }
            }

            return getNodeWithTheLowestFScoreResult;
        }

        private float HeuristicCostEstimate(AStarNode start, AStarNode target) {
            return start.gScore + Mathf.Max(Mathf.Abs(start.position.x - target.position.x),
                                            Mathf.Abs(start.position.y - target.position.y));
        }
    }
}