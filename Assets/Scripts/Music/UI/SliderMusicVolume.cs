﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class SliderMusicVolume : MonoBehaviour, IEndDragHandler {
    private Slider slider;

    private void Awake() {
        slider = GetComponent<Slider>();

        if (PlayerPrefs.HasKey("MusicVolume")) {
            slider.value = PlayerPrefs.GetFloat("MusicVolume");
            OnChangeValue(1);
        } else {
            slider.onValueChanged.AddListener(OnChangeValue);
            OnChangeValue(1);
        }
    }

    private void OnDestroy() {
        slider.onValueChanged.RemoveListener(OnChangeValue);
    }

    private void OnEnable() {
        slider.value = MusicController.Instance.audioSource.volume;
    }

    public void OnChangeValue(float value) {
        if (MusicController.Instance != null) {
            MusicController.Instance.setMusicVolume(slider.value);
            PlayerPrefs.SetFloat("MusicVolume", slider.value);
        }
    }

    public void OnEndDrag(PointerEventData eventData) {
        OnChangeValue(1);
    }
}