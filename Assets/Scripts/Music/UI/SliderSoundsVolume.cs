﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Slider))]
public class SliderSoundsVolume : MonoBehaviour, IEndDragHandler {
    private Slider slider;

    private void Awake() {
        slider = GetComponent<Slider>();

        if (PlayerPrefs.HasKey("SoundsVolume")) {
            slider.value = PlayerPrefs.GetFloat("SoundsVolume");
            OnChangeValue(1);
        } else {
            slider.value = SoundsController.Instance.audioSource.volume;
            OnChangeValue(1);
        }

        slider.onValueChanged.AddListener(OnChangeValue);
    }

    private void OnDestroy() {
        slider.onValueChanged.RemoveListener(OnChangeValue);
    }

    public void OnChangeValue(float value) {
        if (SoundsController.Instance != null) {
            SoundsController.Instance.ChangeVolume(slider.value);
            PlayerPrefs.SetFloat("SoundsVolume", slider.value);
        }
    }

    public void OnEndDrag(PointerEventData eventData) {
        OnChangeValue(1f);
    }
}