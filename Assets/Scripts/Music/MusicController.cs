﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class MusicController : SingleTon<MusicController>
{
    public AudioSource audioSource;

    [SerializeField] private AudioClip menuMusic;
    [SerializeField] private AudioClip gameMusic;

    [SerializeField] private List<string> scenesToPlayMenuMusic;
    [SerializeField] private List<string> scenesToPlayGameMusic;

    public void Awake() {
        if (Instance == this) {
            DontDestroyOnLoad(gameObject);
            audioSource = GetComponent<AudioSource>();
            audioSource.loop = true;

            if (PlayerPrefs.HasKey("MusicVolume")) {
                audioSource.volume = PlayerPrefs.GetFloat("MusicVolume");
            }

            PlayMusicByScene(SceneManager.GetActiveScene().name);
        } else {
            Destroy(gameObject);
        }
    }

    public void setMusicVolume(float volume) {
        audioSource.volume = volume;
    }

    public void PlayMenuMusic() {
        audioSource.clip = menuMusic;
        audioSource.Play();
    }

    public void PlayGameMusic() {
        audioSource.clip = gameMusic;
        audioSource.Play();
    }

    public void PlayMusicByScene(string sceneName) {
        if (sceneName != SceneManager.GetActiveScene().name || audioSource.clip == null) {
            if (scenesToPlayMenuMusic.Contains(sceneName)) {
                PlayMenuMusic();
                return;
            } else if (scenesToPlayGameMusic.Contains(sceneName)) {
                PlayGameMusic();
                return;
            }
        }
    }
}