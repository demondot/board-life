﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(AudioSource))]
public class SoundsController : SingleTon<SoundsController>
{
    public AudioSource audioSource;

    public AudioClip clickSound;

    public Dictionary<AudioClip, IEnumerator> loopedSounds = new Dictionary<AudioClip, IEnumerator>();

    private void Awake() {
        if (Instance == this) {
            DontDestroyOnLoad(gameObject);

            if (PlayerPrefs.HasKey("SoundsVolume")) {
                audioSource.volume = PlayerPrefs.GetFloat("SoundsVolume");
            }

            audioSource = GetComponent<AudioSource>();
        } else {
            Destroy(gameObject);
        }
    }

    public void ChangeVolume(float volume) {
        audioSource.volume = volume;
    }

    public void PlaySoundOnce(AudioClip sound) {
        audioSource.PlayOneShot(sound);
    }

    public void AddLoopedSound(AudioClip sound) {
        if (loopedSounds.ContainsKey(sound) == false) {
            IEnumerator tmpLoopedCorutine = cr_PlaySound(sound);

            loopedSounds.Add(sound, tmpLoopedCorutine);

            StartCoroutine(loopedSounds[sound]);
        }
    }

    public void StopLoopedSound(AudioClip sound) {
        if (loopedSounds.ContainsKey(sound)) {
            StopCoroutine(loopedSounds[sound]);

            audioSource.Stop();

            loopedSounds.Remove(sound);
        }
    }

    public void StopAllLoopedSouns() {
        //StopLoopedSound(FindObjectOfType<Player>().drillSounds);
    }

    private IEnumerator cr_PlaySound(AudioClip audioClip) {
        while (true) {
            audioSource.PlayOneShot(audioClip);
            yield return new WaitForSeconds(audioClip.length);
        }
    }
}