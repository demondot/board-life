﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Button))]
public class UIBtnMenuNavigation : MonoBehaviour, IPointerDownHandler {
    public GameObject objectToHide;
    public GameObject objectToShow;

    public void OnPointerDown(PointerEventData eventData) {
        if(objectToHide != null) objectToHide.SetActive(false);
        if(objectToShow != null) objectToShow.SetActive(true);
    }
}