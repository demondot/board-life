﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class UIInfoScreen : SingleTon<UIInfoScreen>
{
    public GameObject container;
    public RawImage image;
    public TextMeshProUGUI cardName;
    public TextMeshProUGUI cardDescription;
    public TextMeshProUGUI btnName;
    public Button btn;
    public GameCard currentCard;

    [Space]
    public UICardInfo cardInfoPrefub;
    public Transform cardDisabledCradsContainer;
    public Transform cradMoveToContainer;
    public GameObject containerDisableCrads;
    public GameObject containerTeleportTo;

    [Space]
    public GameObject toolTipContainer;
    public TextMeshProUGUI toolTipText;

    [Space]
    public AudioClip goodSound;
    public AudioClip badSound;

    public Action OnButtonOkPressed;

    private Player player;

    private void Start() {
        player = FindObjectOfType<Player>();

        if (player != null) {
            player.aPlayerStoped += OnPlayerStopedMove;
        }
    }

    private void OnDestroy() {
        if (player != null) {
            player.aPlayerStoped -= OnPlayerStopedMove;
        }
    }

    private void OnPlayerStopedMove() {
        if (player.startCard.alreadyMeeted == false && GameField.Instance.finishCard != player.startCard) {
            Show(player.startCard);
        }
    }

    public void Hide() {
        FindObjectOfType<CameraControl>().enabled = true;

        container.SetActive(false);
    }

    public void Show(GameCard card) {
        FindObjectOfType<CameraControl>().enabled = false;

        container.SetActive(true);
        currentCard = card;

        if (GameField.Instance.badCardsConfing.Contains(currentCard.cardType)) {
            SoundsController.Instance.PlaySoundOnce(badSound);
        } else if(GameField.Instance.goodCradsConfig.Contains(currentCard.cardType)) {
            SoundsController.Instance.PlaySoundOnce(goodSound);
        }

        cardDisabledCradsContainer.Clear();
        cradMoveToContainer.Clear();

        // Отобразить карты что отключаются
        if (currentCard.cardType.disableCards.Count > 0) {
            containerDisableCrads.SetActive(true);

            foreach (GameCardType disCardType in currentCard.cardType.disableCards) {
                UICardInfo uiCard = Instantiate(cardInfoPrefub.gameObject, cardDisabledCradsContainer).GetComponent<UICardInfo>();
                uiCard.cardImage.texture = disCardType.cardTexture;
                uiCard.cardType = disCardType;
                uiCard.tooltipContainer = toolTipContainer;
                uiCard.tooltipText = toolTipText;
            }
        } else {
            containerDisableCrads.SetActive(false);
        }

        // Отобразить карты на которые будет совершён переход
        if (currentCard.cardType.ifSuccesfullMoveOnCard.Count > 0 ||
            currentCard.cardType.ifNotSuccesfullMoveOnCard.Count > 0) {
            containerTeleportTo.SetActive(true);
        } else {
            containerTeleportTo.SetActive(false);
        }

        // Если кубики выпадут удачно
        if (currentCard.cardType.ifSuccesfullMoveOnCard.Count > 0) {
            foreach (GameCardType moveCardType in currentCard.cardType.ifSuccesfullMoveOnCard) {
                UICardInfo uiCard = Instantiate(cardInfoPrefub.gameObject, cradMoveToContainer).GetComponent<UICardInfo>();
                uiCard.cardImage.texture = moveCardType.cardTexture;
                uiCard.cardType = moveCardType;
                uiCard.tooltipContainer = toolTipContainer;
                uiCard.tooltipText = toolTipText;
            }
        }

        // Если кубики выпадут неудачно
        if (currentCard.cardType.ifNotSuccesfullMoveOnCard.Count > 0) {
            foreach (GameCardType moveCardType in currentCard.cardType.ifNotSuccesfullMoveOnCard) {
                UICardInfo uiCard = Instantiate(cardInfoPrefub.gameObject, cradMoveToContainer).GetComponent<UICardInfo>();
                uiCard.cardImage.texture = moveCardType.cardTexture;
                uiCard.cardType = moveCardType;
                uiCard.tooltipContainer = toolTipContainer;
                uiCard.tooltipText = toolTipText;
            }
        }

        // ==================================================

        GameCardType cardType = currentCard.cardType;

        image.texture = cardType.cardTexture;
        cardName.text = LocalizationController.keysValues[cardType.cardName];

        if (currentCard.cardType.isNeedToRoll && currentCard.cardType.isConditionToPassEvent) {
            cardDescription.text = LocalizationController.keysValues[cardType.cardDescription].
                Replace("%n", cardType.rollValue.ToString());
        } else {
            cardDescription.text = LocalizationController.keysValues[cardType.cardDescription].
                Replace("%n", Mathf.Abs(cardType.giveHP).ToString());
        }

        currentCard.alreadyMeeted = true;

        if (cardType.isNeedToRoll) {
            btnName.text = LocalizationController.keysValues["%roll%"];
            btn.onClick.RemoveAllListeners();
            btn.onClick.AddListener(ButtonPressedRoll);
        } else {
            btnName.text = LocalizationController.keysValues["%ok%"];
            btn.onClick.RemoveAllListeners();
            btn.onClick.AddListener(ButtonPressedOk);
        }
    }

    private void OpenNearCards() {
        if (player != null) {
            player.OpenRandomNearCards();
            player.DeactivateCards();
            player.SetNearCardsActiveState(true);
        }
    }

    private void MovePlayerOnRundomCardFromList(List<GameCardType> collection) {
        if (player != null && collection.Count > 0) {
            GameCard cardToMoveOn = GameField.Instance.findCardByCardType(
                    collection[UnityEngine.Random.Range(0, collection.Count)]);

            if (cardToMoveOn != null) {
                PlayerControl.Instance.MovePlayerOnCard(cardToMoveOn);
            }
        }
    }

    public void ButtonPressedOk() {
        if(OnButtonOkPressed != null) {
            OnButtonOkPressed();
        }

        OpenNearCards();
        MovePlayerOnRundomCardFromList(player.startCard.cardType.ifSuccesfullMoveOnCard);
        MovePlayerOnRundomCardFromList(player.startCard.cardType.ifNotSuccesfullMoveOnCard);

        Hide();
    }

    public void ButtonPressedRoll() {
        if(DicesController.Instance != null) {
            DicesController.Instance.StartDiceRoll();
        }
        Hide();

        FindObjectOfType<CameraControl>().enabled = false;
    }
}