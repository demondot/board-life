﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;

public class UIDicesScreen : SingleTon<UIDicesScreen>
{
    private Player player;

    public GameObject container;
    public TextMeshProUGUI result;
    public TextMeshProUGUI description;
    public Button okButton;

    void Start()
    {
        player = FindObjectOfType<Player>();
        okButton.onClick.AddListener(OnButtonClick);
        container.SetActive(false);
    }

    public void Show() {
        FindObjectOfType<CameraControl>().enabled = false;

        if (player != null && player.startCard.cardType.isNeedToRoll) {
            container.SetActive(true);

            result.text = LocalizationController.keysValues["%DiceScore%"] + " " + DicesController.Instance.sumValue;

            if (player.startCard.cardType.isConditionToPassEvent) {
                if (player.startCard.cardType.codition == LESS_OR_GREAT.GREAT) {
                    if (DicesController.Instance.sumValue > player.startCard.cardType.rollValue &&
                        LocalizationController.keysValues.ContainsKey(player.startCard.cardType.cardGoodDiceRoll)) {
                        description.text = LocalizationController.keysValues[player.startCard.
                            cardType.cardGoodDiceRoll].Replace("%n",
                            Mathf.Abs(player.startCard.cardType.giveHP).ToString());
                    } else {
                        if (LocalizationController.keysValues.ContainsKey(player.startCard.cardType.cardBadDiceRoll)) {
                            description.text = LocalizationController.keysValues[player.startCard.cardType.cardBadDiceRoll];
                        }
                    }
                } else if (player.startCard.cardType.codition == LESS_OR_GREAT.LESS) {
                    if (DicesController.Instance.sumValue < player.startCard.cardType.rollValue &&
                        LocalizationController.keysValues.ContainsKey(player.startCard.cardType.cardGoodDiceRoll)) {
                        description.text = LocalizationController.keysValues[player.startCard.
                            cardType.cardGoodDiceRoll].Replace("%n",
                            Mathf.Abs(player.startCard.cardType.giveHP).ToString());
                    } else {
                        if (LocalizationController.keysValues.ContainsKey(player.startCard.cardType.cardBadDiceRoll)) {
                            description.text = LocalizationController.keysValues[player.startCard.cardType.cardBadDiceRoll];
                        }
                    }
                }
            } else if (player.startCard.cardType.isGiveHPByDiseResult || player.startCard.cardType.isLoseHPByDiseResult) {
                description.text = LocalizationController.keysValues[player.startCard.
                            cardType.cardGoodDiceRoll].Replace("%n",
                            Mathf.Abs(DicesController.Instance.sumValue).ToString());
            }
        }
    }

    public void Hide() {
        FindObjectOfType<CameraControl>().enabled = true;
        FindObjectOfType<UIBackToPlayer>().enabled = true;
        container.SetActive(false);

        if (DicesController.Instance != null) {
            DicesController.Instance.dice1Camera.gameObject.SetActive(false);
            DicesController.Instance.dice2Camera.gameObject.SetActive(false);
        }
    }

    private void OpenNearCards() {
        if (player != null) {
            player.OpenRandomNearCards();
            player.DeactivateCards();
            player.SetNearCardsActiveState(true);
        }
    }

    private void MovePlayerOnRundomCardFromList(List<GameCardType> collection) {
        if (player != null && collection.Count > 0) {
            GameCard cardToMoveOn = GameField.Instance.findCardByCardType(
                    collection[Random.Range(0, collection.Count)]);

            if (cardToMoveOn != null) {
                PlayerControl.Instance.MovePlayerOnCard(cardToMoveOn);
            }
        }
    }

    public void OnButtonClick() {
        if (player != null && player.startCard.cardType.isNeedToRoll) {
            if (player.startCard.cardType.isConditionToPassEvent) {
                if (player.startCard.cardType.codition == LESS_OR_GREAT.GREAT) {
                    if (DicesController.Instance.sumValue > player.startCard.cardType.rollValue) {
                        LivesController.Instance.ChangeLifeAsCardBonuse();

                        // Двигать игрока на случайную карту из списка
                        MovePlayerOnRundomCardFromList(player.startCard.cardType.ifSuccesfullMoveOnCard);
                    } else {
                        // Двигать игрока на случайную карту из списка
                        MovePlayerOnRundomCardFromList(player.startCard.cardType.ifNotSuccesfullMoveOnCard);
                    }
                } else if (player.startCard.cardType.codition == LESS_OR_GREAT.LESS) {
                    if (DicesController.Instance.sumValue < player.startCard.cardType.rollValue) {
                        LivesController.Instance.ChangeLifeAsCardBonuse();

                        // Двигать игрока на случайную карту из списка
                        MovePlayerOnRundomCardFromList(player.startCard.cardType.ifSuccesfullMoveOnCard);
                    } else {
                        // Двигать игрока на случайную карту из списка
                        MovePlayerOnRundomCardFromList(player.startCard.cardType.ifNotSuccesfullMoveOnCard);
                    }
                }
            } else if (player.startCard.cardType.isLoseHPByDiseResult) {
                LivesController.Instance.AddYeardToLive(DicesController.Instance.sumValue * -1);
            } else if (player.startCard.cardType.isGiveHPByDiseResult) {
                LivesController.Instance.AddYeardToLive(DicesController.Instance.sumValue);
            }
        }

        Destroy(DicesController.Instance.dice1.gameObject);
        Destroy(DicesController.Instance.dice2.gameObject);

        OpenNearCards();

        Hide();
    }
}
