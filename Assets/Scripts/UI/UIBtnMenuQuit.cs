﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Button))]
public class UIBtnMenuQuit : MonoBehaviour, IPointerDownHandler {
    public void Start() {
        if(Application.platform == RuntimePlatform.WebGLPlayer) {
            gameObject.SetActive(false);
        }
    }

    public void OnPointerDown(PointerEventData eventData) {
        if (Application.platform == RuntimePlatform.WindowsEditor) {
            #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            #endif
        } else {
            Application.Quit();
        }
    }
}
