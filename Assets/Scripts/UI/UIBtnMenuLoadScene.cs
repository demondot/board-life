﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Button))]
public class UIBtnMenuLoadScene : MonoBehaviour, IPointerDownHandler {
    public string sceneName;

    public void OnPointerDown(PointerEventData eventData) {
        SceneManager.LoadScene(sceneName);
    }
}
