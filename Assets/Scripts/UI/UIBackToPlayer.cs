﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBackToPlayer : MonoBehaviour
{
    public Button btnBackToPlayer;
    public Camera mainCamera;

    private Player player;

    private void Start() {
        player = FindObjectOfType<Player>();

        if (btnBackToPlayer != null) {
            btnBackToPlayer.onClick.AddListener(onButtonBackPressed);
        }
    }

    private void OnDestroy() {
        if(btnBackToPlayer != null) {
            btnBackToPlayer.onClick.RemoveListener(onButtonBackPressed);
        }
    }

    private void Update() {
        if (enabled == false && btnBackToPlayer != null) {
            if (enabled == false && btnBackToPlayer.gameObject.activeSelf) {
                btnBackToPlayer.gameObject.SetActive(false);
            }
        } else if (enabled && player != null && mainCamera != null && btnBackToPlayer != null) {
            if(isPointInCamView(player.transform.position) && btnBackToPlayer.gameObject.activeSelf) {
                btnBackToPlayer.gameObject.SetActive(false);
            } else if (!isPointInCamView(player.transform.position) && !btnBackToPlayer.gameObject.activeSelf) {
                btnBackToPlayer.gameObject.SetActive(true);
            }
        }
    }

    private void onButtonBackPressed() {
        if (mainCamera != null && player != null && player.startCard != null) {
            mainCamera.transform.position = player.startCard.transform.position +
                                            (Vector3.up * 6) + (Vector3.back * 3);
            mainCamera.transform.LookAt(player.startCard.transform.position);
        }
    }

    private bool isPointInCamView(Vector3 itemPosition) {
        Vector3 screenPoint = mainCamera.WorldToViewportPoint(itemPosition);
        bool onScreen = screenPoint.z > 0 &&
                        screenPoint.x > 0 &&
                        screenPoint.x < 1 &&
                        screenPoint.y > 0 &&
                        screenPoint.y < 1;

        return onScreen;
    }
}