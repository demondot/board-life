﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class UICardInfo : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public RawImage cardImage;
    public GameCardType cardType;
    public GameObject tooltipContainer;
    public TextMeshProUGUI tooltipText;

    public void OnPointerEnter(PointerEventData eventData) {
        tooltipContainer.SetActive(true);
        tooltipText.text = LocalizationController.keysValues[cardType.cardName];
    }

    public void OnPointerExit(PointerEventData eventData) {
        tooltipContainer.SetActive(false);
    }
}