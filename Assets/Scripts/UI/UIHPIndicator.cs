﻿using UnityEngine;
using TMPro;

public class UIHPIndicator : MonoBehaviour
{
    public TextMeshProUGUI text;

    public void Start() {
        LivesController.Instance.aValueWasChanged += UpdateHP;
        UpdateHP();
    }

    public void OnDestroy() {
        if(LivesController.Instance != null) {
            LivesController.Instance.aValueWasChanged -= UpdateHP;
        }
    }

    public void UpdateHP() {
        text.text = LivesController.Instance.currentLives.ToString();
    }
}
