﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Button))]
public class InteractiveButton : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler {
    private Button button;

    private Vector3 tmpLocalScale;

    public void Awake() {
        button = GetComponent<Button>();
    }

    public void OnPointerDown(PointerEventData eventData) {
        if (SoundsController.Instance != null && button.interactable) {
            SoundsController.Instance.PlaySoundOnce(SoundsController.Instance.clickSound);
        }

        button.GetComponent<RectTransform>().localScale = tmpLocalScale;
    }

    public void OnPointerEnter(PointerEventData eventData) {
        tmpLocalScale = button.GetComponent<RectTransform>().localScale;

        button.GetComponent<RectTransform>().localScale *= 1.03f;
    }

    public void OnPointerExit(PointerEventData eventData) {
        button.GetComponent<RectTransform>().localScale = tmpLocalScale;
    }
}
