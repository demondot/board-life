﻿using UnityEngine;
using UnityEngine.UI;

public class UIPauseScreen : MonoBehaviour
{
    public GameObject container;

    public Button btnContinue;
    public Button btnToMainMenu;

    public void Start() {
        btnContinue.onClick.AddListener(Hide);
    }

    public void OnDestroy() {
        btnContinue.onClick.RemoveListener(Hide);
    }

    public void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            SoundsController.Instance.PlaySoundOnce(SoundsController.Instance.clickSound);

            if (container.activeSelf == false) {
                Show();
            } else {
                Hide();
            }
        }
    }

    public void Show() {
        FindObjectOfType<CameraControl>().enabled = false;

        container.SetActive(true);
    }

    public void Hide() {
        FindObjectOfType<CameraControl>().enabled = true;

        container.SetActive(false);
    }
}