﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Button))]
public class UIBtnMenuChangeLanguage : MonoBehaviour, IPointerDownHandler {
    public SystemLanguage language;

    public void OnPointerDown(PointerEventData eventData) {
        LocalizationController.Instance.Init(language);
    }
}
