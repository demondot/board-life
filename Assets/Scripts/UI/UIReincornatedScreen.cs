﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIReincornatedScreen : SingleTon<UIReincornatedScreen>
{
    public GameObject container; 
    public Button btnVictory;
    public RawImage cardImg;

    private Player player;

    [SerializeField] private AudioClip reincornationSound;

    public void Start() {
        player = FindObjectOfType<Player>();

        if(player != null) {
            player.aPlayerStoped += OnPlayerStopedMove;
        }

        if (btnVictory != null) {
            btnVictory.onClick.AddListener(OnButonOk);
        }
    }

    public void OnDestroy() {
        if (player != null) {
            player.aPlayerStoped -= OnPlayerStopedMove;
        }

        if (btnVictory != null) {
            btnVictory.onClick.RemoveListener(OnButonOk);
        }
    }

    public void Show() {
        SoundsController.Instance.PlaySoundOnce(reincornationSound);

        FindObjectOfType<CameraControl>().enabled = false;

        container.SetActive(true);

        cardImg.texture = player.startCard.cardType.cardTexture;
    }

    public void Hide() {
        FindObjectOfType<CameraControl>().enabled = true;

        container.SetActive(false);
    }

    private void OnPlayerStopedMove() {
        if (GameField.Instance.finishCard == player.startCard) {
            Show();
        }
    }

    public void OnButonOk() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        Hide();
    }
}