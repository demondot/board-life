﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class UILoseScreen : SingleTon<UILoseScreen>
{
    public GameObject container;
    public Button btnLose;
    public TextMeshProUGUI loseText;

    public AudioClip loseSound;

    public void Start() {
        if (LivesController.Instance != null) {
            LivesController.Instance.aValueWasChanged += OnLiveWasChanged;
        }

        if (btnLose != null) {
            btnLose.onClick.AddListener(OnButonLose);
        }
    }

    public void OnDestroy() {
        if (LivesController.Instance != null) {
            LivesController.Instance.aValueWasChanged -= OnLiveWasChanged;
        }

        if (btnLose != null) {
            btnLose.onClick.RemoveListener(OnButonLose);
        }
    }

    public void Show() {
        SoundsController.Instance.PlaySoundOnce(loseSound);

        FindObjectOfType<CameraControl>().enabled = false;
        container.SetActive(true);

        loseText.text = LocalizationController.keysValues["%lose_text%"].Replace("%n", PlayerPrefs.GetInt("SCORE").ToString());
    }

    public void Hide() {
        FindObjectOfType<CameraControl>().enabled = true;
        container.SetActive(false);
    }

    private void OnLiveWasChanged() {
        if (LivesController.Instance.currentLives < 0) {
            Show();
        }
    }

    public void OnButonLose() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        PlayerPrefs.SetInt("SCORE", 0);

        Hide();
    }
}