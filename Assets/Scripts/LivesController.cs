﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LivesController : SingleTon<LivesController>
{
    public int currentLives = 70;
    private Player player;

    public Action aValueWasChanged;

    private void Start() {
        player = FindObjectOfType<Player>();

        player.aPlayerStoped += OnPlayerStopedMove;

        if (UIInfoScreen.Instance != null) {
            UIInfoScreen.Instance.OnButtonOkPressed += OnButtonOkWasPressed;
        }
    }

    private void OnDestroy() {
        if (player != null) {
            player.aPlayerStoped -= OnPlayerStopedMove;
        }

        if (UIInfoScreen.Instance != null) {
            UIInfoScreen.Instance.OnButtonOkPressed -= OnButtonOkWasPressed;
        }
    }

    public void MakeStep() {
        currentLives -= 1;

        PlayerPrefs.SetInt("SCORE", PlayerPrefs.GetInt("SCORE") + 1);

        if (aValueWasChanged != null) {
            aValueWasChanged();
        }
    }

    public void AddYeardToLive(int years) {
        currentLives += years;

        if (player.startCard.cardType.giveHP < 0) {
            int currentScore = PlayerPrefs.GetInt("SCORE");

            PlayerPrefs.SetInt("SCORE", currentScore +
                        Mathf.Abs(player.startCard.cardType.giveHP));
        }

        if (aValueWasChanged != null) {
            aValueWasChanged();
        }
    }

    public void ChangeLifeAsCardBonuse() {
        currentLives += player.startCard.cardType.giveHP;

        if(player.startCard.cardType.giveHP < 0) {
            int currentScore = PlayerPrefs.GetInt("SCORE");

            PlayerPrefs.SetInt("SCORE", currentScore +
                        Mathf.Abs(player.startCard.cardType.giveHP));
        }

        if (aValueWasChanged != null) {
            aValueWasChanged();
        }
    }

    private void OnButtonOkWasPressed() {
        ChangeLifeAsCardBonuse();
    }

    private void OnPlayerStopedMove() {
        MakeStep();
    }
}